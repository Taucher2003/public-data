# ReactionRole Terms of Service

Currently the ReactionRole project is closed-source. If this changes in the future, these terms stay only applicable to the official instance `ReactionRole#5803`/`664849019654111233`.

By using the ReactionRole bot, you agree to be bound by these terms and the applicable laws and regulations. If you do not agree with any of these, you can't use the ReactionRole bot.

## Limitations

The service is provided "as is". In no event shall I be liable for any damages or data loss arising out of the use of this service.

You are not allowed to abuse or trying to bypass any limitations enforced by this service.

This service is provided with limited availability. It can shutdown at any time. In case of deactivation of the service we have no obligation to hand over your data to you.

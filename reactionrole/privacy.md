# ReactionRole Privacy Policy

Currently the ReactionRole project is closed-source. If this changes in the future, this policy stays only applicable to the official instance `ReactionRole#5803`/`664849019654111233`.

Privacy of data is important. No data will be shared publicly or with third parties. Additionally the bot only stores data which is required to provide it's service.

Disclaimer: This document is for reference only. It does not claim to be legally valid or applicable.

## What of your data is stored?

<sub>Note: This list is not guaranteed to be up to date. There may be a delay between changes in the code and the update of this document.</sub> \
<sub>Note: This list does not include data, which is only used internally by the bot.</sub>

### Reaction Roles

Reaction Roles are the main feature of the bot. To provide this feature, we store the following for each Reaction Role you have set up on your server:

- Your Guild ID
- The ID of the channel
- The ID of the message
- The ID of the role
- The Emote
- (+ 2 internal data entries)

### Reaction Role Settings

Reaction Role Settings are settings per guild. There is one entry for each guild with the following data:

- Your Guild ID
- A custom message for added roles (deprecated feature)
- A custom message for removed roles (deprecated feature)
- A channel ID for logs (deprecated feature)
- (+ 2 internal data entries)

### Reaction Groups

Reaction Groups are used to group Reaction Roles and apply settings to them as a group. Each Reaction Group contains the following data:

- Your Guild ID
- The ID of the header role
- (+ 3 internal data entries)
